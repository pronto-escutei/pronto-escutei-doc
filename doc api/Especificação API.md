# Especificação API

## Autentificação

* Login(login, senha)
* cadastro( usuario )

## usuario

* listar()
* alterar( usuario )
* pedirAmizade( IdUsuario )
* aceitarAmizade( IdUsuario )

## post

* listar()
* listarPorAmizade()
* listar( loja )
* listar( produto )
* criar( conteudo, loja, produto )
* alterar( post, conteudo, loja, produto )
* deletar( post )
* avaliar( post, avaliacao )

## comentario

* listar( post )
* alterar( post, conteudo )
* criar( post, conteudo )

## loja

* listar()
* criar( loja )

## produto

* listar
* criar( loja, produto )

## avaliar

* comentario( comentario )
* post( post )
