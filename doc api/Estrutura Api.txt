config -> configuração de projeto
controller -> controladorcontrola entrada e saida da api
dominio -> espelho das classes do banco
dto -> objeto de tranferencia da api
infra -> todas operação eu envolva banco de dados
middleware -> intercepção dos controllers (exemplo: auth verifica se usuário tá logado)
routers -> são as rotas da aplicação ( todas endpoints ) /* removido graças ao tsoa */
service -> todo o serviço da aplicação
utils -> qualquer classe que pode ser usada no projeto em geral

extras: 

dist -> arquivos em js gerados pelo javascript e que serão executados pelo node
spec -> todos os arquivos de testes