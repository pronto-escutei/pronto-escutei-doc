# breve manual

## pré requisitos

- node
- npm
- typescript
- insomnia (opcional testar as rotas)

## inicializar projeto

`npm install`

## rodar projeto com alteração do arquivo

`npm run watch`

## rodar somente a api

`npm start`

## compilação typescript

`tsc`

## rodar testes

`jasmine`

## gerar documentacao e novos controllers

`npm run generate`

## link da documentação

[text](https://localhost:3000/api-docs/), obs: não é possivel autentificar pelo swagger

## consumir endpoint autentificado

passar no header authorization: Bearer **(token autentificado)**
