
-- USUARIO
create table uclpi.usuario(id int primary key, nome varchar(100), email varchar(100), senha varchar(100), id_facebook varchar(100), salt varchar(100))
CREATE SEQUENCE uclpi.usuario_sequence START WITH 1

-- AMIZADE
create table uclpi.amizade(id int, solicitante int, solicitado int, aceito char check (aceito in (0,1)),
    CONSTRAINT fk_amizade_usuario_solicitante FOREIGN KEY (solicitante) REFERENCES uclpi.usuario (id),
    CONSTRAINT fk_amizade_usuario_solicitado FOREIGN KEY (solicitado) REFERENCES uclpi.usuario (id)
)
CREATE SEQUENCE uclpi.amizade_sequence START WITH 1

-- LOJA
create table uclpi.loja(id int primary key, nome varchar(100), link varchar(300), imagem varchar(300));
CREATE SEQUENCE uclpi.loja_sequence START WITH 1;

-- PRODUTO
create table uclpi.produto(id int primary key, loja_id int, link varchar(300), nome varchar(50), imagem varchar(300),
    CONSTRAINT fk_produto_loja FOREIGN KEY (loja_id) REFERENCES uclpi.loja (id)
)
CREATE SEQUENCE uclpi.produto_sequence START WITH 1

-- POST
create table uclpi.post(id int primary key, conteudo varchar(300), usuario_id int,  produto_id int,
    CONSTRAINT fk_post_usuario FOREIGN KEY (usuario_id) REFERENCES uclpi.usuario (id),
    CONSTRAINT fk_post_produto FOREIGN KEY (produto_id) REFERENCES uclpi.produto (id)
)
CREATE SEQUENCE uclpi.post_sequence START WITH 1

-- COMETARIO
create table uclpi.comentario(id int primary key, post_id int, conteudo varchar(300),
    CONSTRAINT fk_comentario_post FOREIGN KEY (post_id) REFERENCES uclpi.post (id),
    CONSTRAINT fk_comentario_usuario FOREIGN KEY (usuario_id) REFERENCES uclpi.usuario (id)
)
CREATE SEQUENCE uclpi.comentario_sequence START WITH 1

-- LIKEPOST
create table uclpi.likePost(id int primary key, post_id int, positivo char(1),
    CONSTRAINT fk_likePost_post FOREIGN KEY (post_id) REFERENCES uclpi.post (id),
    CONSTRAINT fk_likePost_usuario FOREIGN KEY (usuario_id) REFERENCES uclpi.usuario (id)
)
CREATE SEQUENCE uclpi.likePost_sequence START WITH 1

-- LIKECOMENTARIO
create table uclpi.likeComentario(id int primary key, Comentario_id int, positivo char(1),
    CONSTRAINT fk_likeComentario_comentario FOREIGN KEY (comentario_id) REFERENCES uclpi.comentario (id),
    CONSTRAINT fk_likeComentario_usuario FOREIGN KEY (usuario_id) REFERENCES uclpi.usuario (id)
)
CREATE SEQUENCE uclpi.likeComentario_sequence START WITH 1

-- ADICIONAL
create table uclpi.adicional(id int primary key, post_id int, conteudo varchar(300),
    CONSTRAINT fk_adicional_post FOREIGN KEY (post_id) REFERENCES uclpi.post (id)
)
CREATE SEQUENCE uclpi.adicional_sequence START WITH 1